+++
title = "C and C++ on Windows"
description = "How to build and run C and C++ code on Windows"

[extra]
icon = " "
+++

## Introduction

In the 1970s a man named Dennis Ritchie, working at Bell Labs, created a programming language called C to help his coworker Ken Thompson write his new operating system that would become called "UNIX". The vast majority of modern operating systems are based on UNIX. As such, it tends to be relatively easy to write C on most modern operating systems. The exception is Windows. **Windows is so fundumentally different from UNIX that writing C on it tends to be difficult.**

At some point people created a programming language called "C with Objects". It should go without saying that this is basically C with the ability to use object-oriented models. As time went on more fundumental changes were made to the language and we eventually ended up with C++. Unfortunately C++ retained enough of it's C library and build system that it too is a pain to work with on Windows.

The important thing to understand is that Windows has it's own version of C++ that is significantly different from standard C++. It has the advantage that it works well with Windows, but the obvious disadvantage that it's not really C++. I'll be calling this Windows C++.

However, it is possible to get C and/or C++ working on Windows using some clever methods and tricks. I've listed some of the best ones I've discovered over the years in this article.

It's also worth noting that the Git Bash and WSL methods will require a separate plain text editor. You're always welcome to use Vim or something (you nerd), or you could use a graphical one. There's always the classic Notepad++, or the modern VS Code, but I'd recommend trying Kate. The latest build for Windows can be found in the Windows Store for free.

## 󰊢 Using Git Bash

- Languages: C and C++
- Commandline: Yes (Bash)

Shockingly, one of the most functional solutions is to install Git for Windows, and use the little Bash emulator that comes with it. It comes with many little UNIX utilities, which for some reason includes `gcc` and `g++`. This has the obvious advantage of being real C++. However, it does require using the command line to compile it and run it, which can be an advantage or a disadvantage depending on your familiarity with the command line.

Your code can be built with the following commands:

```bash
cd <path to source code directory>
gcc -o <executable name> <code>.c   # Compile the code. Change gcc to g++ to compile C++.
./<executable name>                 # Run the compiled executable.
```

See [Building C](/programming/building-c) (coming soon) or [Building C++](/programming/building-cpp) (coming soon) for more info.

##  Using WSL

- Languages: C and C++
- Commandline: Yes (Bash)

Probably my preferred solution is to use WSL2 (Windows Subsystem for Linux version 2). This has the same advantages and disadvantages as using Git Bash, with some extras. WSL is basically a Linux VM/hypervisor that is somewhat light compared to most, but is still somewhat heavy on battery life and RAM. That said, you do get the benefits of a full, real Linux shell (usually Bash) with full support for all the standard C and C++ things. You're the least likely to run into problems using this method.

This method will also be the easiest for using CMake or other complex build systems, which makes it easier to work with libraries other than the standard library.

## Using Qt Creator

- Languages: C++ only
- Commandline: No

By far the quirkiest and most clever solution is not one I thought of. A friend of mine who is not good with the command line but wanted to write real C++ discovered a fun little way to get full C++ without needing Bash, all in the same IDE. You can simply use Qt Creator and ignore the Qt part. The advantages are clear to someone who likes IDEs. However, the Qt part, even if ignored, sometimes manages to make code behave kind of weirdly. Qt Creator also doesn't have the best text editing capabilities and is a pain to get a hold of on Windows (ironically).

##  Visual Studio and Windows C++

- Languages: C++(ish) only
- Commandline: No

This solution is what UCCS tells it's computer science students who run Windows to do. Basically, download Visual Studio (not Code) Community Edition, and set up a C++ project. Easy as that... except that C++ code from the textbook and from w3schools often doesn't compile. This is because Visual Studio uses the old Window's C++, not standard C++ as we know it. This the #1 cause of problems, by far and away, for students who run Windows in C/C++ classes at UCCS.

Good luck, have fun!!!
